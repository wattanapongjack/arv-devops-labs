Prerequisite

```
sudo -i
npm install -g npm@10.5.0
npm i -g serverless

```

Deploy Services

```
serverless deploy

```

Remove Services

```
serverless remove

```